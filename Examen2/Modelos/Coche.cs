﻿using System;

namespace Examen2
{
	public class Coche
	{
		public string marca {
			get;
			set;
		}
		public string modelo {
			get;
			set;
		}
		public string color {
			get;
			set;
		}
		public int precio {
			get;
			set;
		}

		public Coche (string marca, string modelo, string color, int precio)
		{
			this.marca = marca;
			this.modelo = modelo;
			this.color = color;
			this.precio = precio;
		}
	}
}


