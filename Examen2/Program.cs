﻿using System;
using DbLinq;
using System.Collections.Generic;
using System.Linq;

namespace Examen2
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			
			Console.WriteLine ("Vamos a usar DATALINQ!");

			var saca = new Consultor ();
			var coches = new Coches ();
			var dic = new Diccionario ();

		    //Console.WriteLine (saca.getSaca ());

			var p1 = new Persona ("Juan", "Gonzalez", 14, true);
			var p2 = new Persona ("Maria", "Lopez", 23, true);
			var p3 = new Persona ("Pepe", "Guitierrez", 67, false);
			var p4 = new Persona ("Candido", "Jackson", 45, true);
			var p5 = new Persona ("Marta", "Lopez", 29, false);
			var p6 = new Persona ("Maria", "Lopez", 8, true);

			Coche b = new Coche ("BMW", "M4", "AZUL", 95360);
			Coche m = new Coche ("MERCEDES", "S65 AMG", "SILVER ADAMANTIUNM", 190256);
			Coche r = new Coche ("RENAULT", "MEGANE", "BLANCO", 21250);

			saca.paLaSaca (p1);
			saca.paLaSaca (p2);
			saca.paLaSaca (p3);
			saca.paLaSaca (p4);
			saca.paLaSaca (p5);
			saca.paLaSaca (p6);

			coches.addCoches (b);
			coches.addCoches (m);
			coches.addCoches (r);

			dic.setDiccionario (p1, coches.getCoches ());
			dic.setDiccionario (p2, coches.getCoches ());

			var resultadoCasado = saca.getSaca ().Where (p => p.casado == true).OrderBy(p=> p.edad);
			var resultadoApellido = saca.getSaca ().Where (p => p.apellido == "Lopez").OrderBy (p => p.edad).Select (p => p.nombre);

			//var cochesMario= dic.getDiccionario().Where(p=> p.Key.nombre == "Mario"); NO TIENE SENTIDO

		
			//Console.WriteLine(cochesMario.ToString());
			

			foreach (Persona x in resultadoCasado) {
			
				Console.WriteLine ("Personas casadas ordenadas por edad: " + x.nombre + " " + x.apellido + " tiene " + x.edad);


			}

			for (int i = 0; i < 3; ++i) {

				Console.WriteLine ("");

			}

			foreach (var x in resultadoApellido) {

				Console.WriteLine ("Personas con mismo apellido ordenadas por edad: " + x);


			}
					
		
		}
	}
}
