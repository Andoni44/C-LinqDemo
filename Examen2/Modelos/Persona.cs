﻿using System;

namespace Examen2
{
	public class Persona
	{
		public Persona ()
		{
			
		}

		public Persona(string nombre, string apellido, ushort edad, Boolean casado)
		{
			this.edad = edad;
			this.casado = casado;
			this.nombre = nombre;
			this.apellido = apellido;

		}

		public string nombre{

			get;
			set;
		}

		public string apellido {

			get;
			set;
		}

		public ushort edad {

			get;
			set;

		}

		public Boolean casado {


			get;
			set;
		}
	}
}

