﻿using System;
using System.Collections.Generic;
namespace Examen2
{
	public class Diccionario
	{
		public Diccionario ()
		{
			this.diccionario = new Dictionary<Persona,List<Coche>> ();
		}
		public Dictionary<Persona,List<Coche>> getDiccionario()
		{
			return this.diccionario;
		}
		public void setDiccionario(Persona p, List<Coche> c)
		{
			this.diccionario.Add (p, c);
		}
		private Dictionary<Persona,List<Coche>> diccionario;
	}
}

