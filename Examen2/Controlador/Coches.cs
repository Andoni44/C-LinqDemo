﻿using System;
using System.Collections.Generic;
namespace Examen2
{
	public class Coches
	{

		private List<Coche> listCoches;

		public Coches(){
			this.listCoches= new List<Coche>();
		}

		public void addCoches (Coche c)
		{
			this.listCoches.Add (c);
		}

		public List<Coche> getCoches(){
			return this.listCoches;
		}
	}
}

